import sys
import time
import os
import pyautogui as ptg
from win32gui import GetWindowText, GetForegroundWindow
import pyperclip

# vars
font = 'GOST type A'
firstMB = 'warning'
pyperclip.copy('GOST type A')
timeout = 15

while firstMB != 'Cancel' or firstMB != 'Done':
    x1 = 1
    x2 = 1
    x3 = 1
    firstMB = ptg.confirm(title='AltiumBot', buttons=['Старт', 'Калибровка', 'Папка с файлами', 'Cancel'],
                          text='Выберите действие\n'
                               '"Старт" запускает скрипт со старыми координатами.\n'
                               '"Калибровка" перезапишет координаты кнопок в Альтиуме.\n'
                               '"Папка с файлами" изменит путь к Sch библиотеке и запишет в текстовый файл'
                               ' все позиции по которым надо пройтись.')
    if firstMB == 'Старт':
        with open('coordinates.txt', 'r') as f:     # прочитали координаты и записали со смещением
            f.readline()
            font1 = int(f.readline()), int(f.readline())
            font1Sort = (font1[0], font1[1] - 20)
            f.readline()
            f.readline()

            font2 = int(f.readline()), int(f.readline())
            font2Sort = (font2[0], font2[1] - 20)
            f.readline()
            f.readline()

            font3 = int(f.readline()), int(f.readline())
            font3Sort = (font3[0], font3[1] - 20)
            # сделать что-то с багом альтиума, когда окно конкретных компонентов просто не открывается до перезапуска
                                                # сделать лог, какие файлы были отредактированы, сколько позиций и тд.
        with open('files.txt', 'r') as files:   # сделать исключение папок, одна папка в середине списка всё испортит
            path = files.readline()[:-1]  # файл хранит путь к папке sch в первой строчке
            FileNameList = os.listdir(path)
            for i in range(len(FileNameList)):
                FileNameList[i] = path + '\\' + FileNameList[i]
            os.startfile(FileNameList[0])       # позже должна открывать в цикле все 549 позиций

        if ptg.confirm('Все выделения в альтиуме должны быть сняты, окна закрыты.\n'
                       'Нажмите клавиатурой OK, чтобы продолжить выполнение скрипта, мышь не двигать\n'
                       '(Используются сохранённые координаты)\n'
                       'ОБЯЗАТЕЛЬНО ВЫБРАТЬ АНГЛИЙСКУЮ РАСКЛАДКУ') != 'OK':
            sys.exit()

        ptg.click(font1Sort, clicks=2, interval=0.5)  # отсортировали
        # ИСПРАВИТЬ СРАВНЕНИЕ С АЛФАВИТОМ БУФЕРА ОБМЕНА, некоторые шрифты просто не отображаются и неизвестно что будет
        # ПРЕДУСМОТРЕТЬ СКРОЛЛ ЭКРАНА (при 50+ строчек)
        time.sleep(0.3)
        while x1 == 1:
            AltiumWindow = GetForegroundWindow()      # записали окно альтиума (для каждого компонента будет свой айди)
            ifout = 0
            ptg.click(font1, clicks=3, interval=0.2)
            while GetWindowText(GetForegroundWindow()) != 'Font' and ifout < timeout:
                time.sleep(0.1)
                ifout = ifout + 1           # периодично спит пока не откроется новое окно (Fonts)
            if ifout < timeout:
                activeWindow = GetForegroundWindow()  # записали айди открывшегося окна
                pyperclip.copy('Warning')   # если в строке пусто, следующая строка ничего не сделает
                with ptg.hold('ctrl'):      # скопировали шрифт под курсором
                    ptg.press('c')
                if pyperclip.paste() == 'Warning':
                    print('Шрифт не отображается и не был скопирован, возможен сбой редактирования')
                if pyperclip.paste() < font:
                    if GetWindowText(GetForegroundWindow()) == 'Font' and activeWindow == GetForegroundWindow():
                        pyperclip.copy(font)
                        with ptg.hold('ctrl'):
                            ptg.press('v')
                        time.sleep(0.2)
                        ptg.hotkey('Enter')
                        time.sleep(0.1)
                        if AltiumWindow == GetForegroundWindow():
                            font1 = (font1[0], font1[1] + 21)
                            time.sleep(0.3)
                        elif GetWindowText(GetForegroundWindow()) == 'Font' and activeWindow == GetForegroundWindow():
                            print('По какой-то причине окно со шрифтами не закрылось, доп окна не обнаружены')
                            ptg.hotkey('Esc')
                        elif GetWindowText(GetForegroundWindow()) == 'Font' and activeWindow != GetForegroundWindow():
                            ptg.alert('Какое-то другое окно было открыто за последние секунды скрипта, выходим.')
                            sys.exit()  # здесь должна быть запись в логи files, если вдруг что-то пошло не так
                elif pyperclip.paste() > font or pyperclip.paste() == 'Warning':
                    if GetWindowText(GetForegroundWindow()) == 'Font' and activeWindow == GetForegroundWindow():
                        pyperclip.copy(font)
                        with ptg.hold('ctrl'):
                            ptg.press('v')
                        time.sleep(0.2)
                        ptg.hotkey('Enter')
                        time.sleep(0.1)
                        if AltiumWindow == GetForegroundWindow():
                            font1 = (font1[0], font1[1])
                            time.sleep(0.3)
                        elif GetWindowText(GetForegroundWindow()) == 'Font' and activeWindow == GetForegroundWindow():
                            print('По какой-то причине окно со шрифтами не закрылось, доп окна не обнаружены')
                            ptg.hotkey('Esc')
                        elif GetWindowText(GetForegroundWindow()) == 'Font' and activeWindow != GetForegroundWindow():
                            ptg.alert('Какое-то другое окно было открыто за последние секунды скрипта, выходим.')
                            sys.exit()  # здесь должна быть запись в логи files, если вдруг что-то пошло не так
                    ptg.click(font1Sort, clicks=2, interval=0.5)
                    time.sleep(0.15)
                elif pyperclip.paste() == font:
                    ptg.hotkey('Esc')
                    font1 = (font1[0], font1[1] + 21)
                    time.sleep(0.3)
            elif ifout == timeout:       # завершает работу с Font если не открылось окно редактирования, переходит дальше
                x1 = x1 + 1

        ptg.click(font2Sort, clicks=2, interval=0.5)  # отсортировали
        time.sleep(0.3)
        while x2 == 1:
            AltiumWindow = GetForegroundWindow()  # записали окно альтиума (для каждого компонента будет свой айди)
            ifout = 0
            ptg.click(font2, clicks=3, interval=0.2)
            while GetWindowText(GetForegroundWindow()) != 'Font' and ifout < timeout:
                time.sleep(0.1)
                ifout = ifout + 1  # периодично спит пока не откроется новое окно (Fonts)
            if ifout < timeout:
                activeWindow = GetForegroundWindow()  # записали айди открывшегося окна
                pyperclip.copy('Warning')  # если в строке пусто, следующая строка ничего не сделает
                with ptg.hold('ctrl'):  # скопировали шрифт под курсором
                    ptg.press('c')
                if pyperclip.paste() == 'Warning':
                    print('Шрифт не отображается и не был скопирован, возможен сбой редактирования')
                if pyperclip.paste() < font:
                    if GetWindowText(GetForegroundWindow()) == 'Font' and activeWindow == GetForegroundWindow():
                        pyperclip.copy(font)
                        with ptg.hold('ctrl'):
                            ptg.press('v')
                        time.sleep(0.2)
                        ptg.hotkey('Enter')
                        time.sleep(0.1)
                        if AltiumWindow == GetForegroundWindow():
                            font2 = (font2[0], font2[1] + 21)
                            time.sleep(0.3)
                        elif GetWindowText(GetForegroundWindow()) == 'Font' and activeWindow == GetForegroundWindow():
                            print('По какой-то причине окно со шрифтами не закрылось, доп окна не обнаружены')
                            ptg.hotkey('Esc')
                        elif GetWindowText(GetForegroundWindow()) == 'Font' and activeWindow != GetForegroundWindow():
                            ptg.alert('Какое-то другое окно было открыто за последние секунды скрипта, выходим.')
                            sys.exit()  # здесь должна быть запись в логи files, если вдруг что-то пошло не так
                elif pyperclip.paste() > font or pyperclip.paste() == 'Warning':
                    if GetWindowText(GetForegroundWindow()) == 'Font' and activeWindow == GetForegroundWindow():
                        pyperclip.copy(font)
                        with ptg.hold('ctrl'):
                            ptg.press('v')
                        time.sleep(0.2)
                        ptg.hotkey('Enter')
                        time.sleep(0.1)
                        if AltiumWindow == GetForegroundWindow():
                            font2 = (font2[0], font2[1])
                            time.sleep(0.3)
                        elif GetWindowText(GetForegroundWindow()) == 'Font' and activeWindow == GetForegroundWindow():
                            print('По какой-то причине окно со шрифтами не закрылось, доп окна не обнаружены')
                            ptg.hotkey('Esc')
                        elif GetWindowText(GetForegroundWindow()) == 'Font' and activeWindow != GetForegroundWindow():
                            ptg.alert('Какое-то другое окно было открыто за последние секунды скрипта, выходим.')
                            sys.exit()  # здесь должна быть запись в логи files, если вдруг что-то пошло не так
                    ptg.click(font2Sort, clicks=2, interval=0.5)
                    time.sleep(0.15)
                elif pyperclip.paste() == font:
                    ptg.hotkey('Esc')
                    font2 = (font2[0], font2[1] + 21)
                    time.sleep(0.3)
            elif ifout == timeout:  # завершает работу с Font если не открылось окно редактирования, переходит дальше
                x2 = x2 + 1

        ptg.click(font3Sort, clicks=2, interval=0.5)  # отсортировали
        time.sleep(0.3)
        while x3 == 1:
            AltiumWindow = GetForegroundWindow()  # записали окно альтиума (для каждого компонента будет свой айди)
            ifout = 0
            ptg.click(font3, clicks=3, interval=0.2)
            while GetWindowText(GetForegroundWindow()) != 'Font' and ifout < timeout:
                time.sleep(0.1)
                ifout = ifout + 1  # периодично спит пока не откроется новое окно (Fonts)
            if ifout < timeout:
                activeWindow = GetForegroundWindow()  # записали айди открывшегося окна
                pyperclip.copy('Warning')  # если в строке пусто, следующая строка ничего не сделает
                with ptg.hold('ctrl'):  # скопировали шрифт под курсором
                    ptg.press('c')
                if pyperclip.paste() == 'Warning':
                    print('Шрифт не отображается и не был скопирован, возможен сбой редактирования')
                if pyperclip.paste() < font:
                    if GetWindowText(GetForegroundWindow()) == 'Font' and activeWindow == GetForegroundWindow():
                        pyperclip.copy(font)
                        with ptg.hold('ctrl'):
                            ptg.press('v')
                        time.sleep(0.2)
                        ptg.hotkey('Enter')
                        time.sleep(0.1)
                        if AltiumWindow == GetForegroundWindow():
                            font3 = (font3[0], font3[1] + 21)
                            time.sleep(0.3)
                        elif GetWindowText(GetForegroundWindow()) == 'Font' and activeWindow == GetForegroundWindow():
                            print('По какой-то причине окно со шрифтами не закрылось, доп окна не обнаружены')
                            ptg.hotkey('Esc')
                        elif GetWindowText(GetForegroundWindow()) == 'Font' and activeWindow != GetForegroundWindow():
                            ptg.alert('Какое-то другое окно было открыто за последние секунды скрипта, выходим.')
                            sys.exit()  # здесь должна быть запись в логи files, если вдруг что-то пошло не так
                elif pyperclip.paste() > font or pyperclip.paste() == 'Warning':
                    if GetWindowText(GetForegroundWindow()) == 'Font' and activeWindow == GetForegroundWindow():
                        pyperclip.copy(font)
                        with ptg.hold('ctrl'):
                            ptg.press('v')
                        time.sleep(0.2)
                        ptg.hotkey('Enter')
                        time.sleep(0.1)
                        if AltiumWindow == GetForegroundWindow():
                            font3 = (font3[0], font3[1])
                            time.sleep(0.3)
                        elif GetWindowText(GetForegroundWindow()) == 'Font' and activeWindow == GetForegroundWindow():
                            print('По какой-то причине окно со шрифтами не закрылось, доп окна не обнаружены')
                            ptg.hotkey('Esc')
                        elif GetWindowText(GetForegroundWindow()) == 'Font' and activeWindow != GetForegroundWindow():
                            ptg.alert('Какое-то другое окно было открыто за последние секунды скрипта, выходим.')
                            sys.exit()  # здесь должна быть запись в логи files, если вдруг что-то пошло не так
                    ptg.click(font3Sort, clicks=2, interval=0.5)
                    time.sleep(0.15)
                elif pyperclip.paste() == font:
                    ptg.hotkey('Esc')
                    font3 = (font3[0], font3[1] + 21)
                    time.sleep(0.3)
            elif ifout == timeout:  # завершает работу с Font если не открылось окно редактирования, переходит дальше
                x3 = x3 + 1
        sys.exit()
        with ptg.hold('ctrl'):  # закрытие документа
            ptg.hotkey('F4')
        time.sleep(0.5)
        ptg.hotkey('Enter')     # сохранили файл
        with open('files.txt', 'r') as f:
            os.startfile(f.readline()[:-1])     # дописать цикл открытия файлов когда скрипт будет готов
        sys.exit()

        firstMB = 'Done'
        ptg.alert('Done')
    elif firstMB == 'Калибровка':
        with open('coordinates.txt', 'w') as f:  # редактируется файл с координатами
            ptg.alert('Наведите курсор в центр "***" столбца "Font" и закройте это окно\n'
                      '(мышь не двигать)')
            font1 = (ptg.position())
            f.write('font1 coordinates:\n')
            f.write(str(font1[0]) + '\n')
            f.write(str(font1[1]) + '\n\n')

            ptg.alert('Наведите курсор в центр "***" столбца "Pin Name Font" и закройте это окно\n'
                      '(мышь не двигать)')
            font2 = (ptg.position())
            f.write('font2 coordinates:\n')
            f.write(str(font2[0]) + '\n')
            f.write(str(font2[1]) + '\n\n')

            ptg.alert('Наведите курсор в центр "***" столбца "Pin Designator Font" и закройте это окно\n'
                      '(мышь не двигать)')
            font3 = (ptg.position())
            f.write('font3 coordinates:\n')
            f.write(str(font3[0]) + '\n')
            f.write(str(font3[1]) + '\n\n')
    elif firstMB == 'Папка с файлами':
        path = ptg.prompt('Укажите путь к папке, где хранятся необходимые для редактирования файлы в формате .SchLib:\n'
                          r'Например: "C:\Users\ilysh\ilysh\pcb_libraries\sh"'
                          '\nВ результате сформируется текстовый файл с полным перечнем файлов, которые будут '
                          'последовательно запускаться по мере работы скрипта', default='Type there')
        if path == 'Type there' or path is None or path == 'Cancel':
            ptg.alert('Ничего не введено, выходим...')
        else:
            x = os.listdir(path)        # список файлов
            with open('files.txt', 'w') as files:
                files.write(path + '\n')       # хранит путь в первой строчке
                for i in range(len(x)): # кол-во строк в списке x (кол-во итоговых файлов)
                    files.write(path + '\\')
                    files.write(str(x[i]) + '\n')
            ptg.alert('Готово\n'
                      'В папке со скриптом сформирован файл с перечнем расположений файлов')
    else:
        sys.exit()
